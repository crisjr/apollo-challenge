from django.http import JsonResponse
from django.shortcuts import render

from xml_converter.api import parse_xml, INVALID_XML_MESSAGE


def upload_page(request):
    if request.method == 'POST':
        xml_file = request.FILES.get("file")
        response = parse_xml(xml_file.read())
        status_code = 400 if response == INVALID_XML_MESSAGE else 200
        return JsonResponse(response, status=status_code, safe=False)

    return render(request, "upload_page.html")
