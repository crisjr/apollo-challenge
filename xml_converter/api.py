import xml.etree.ElementTree as etree

from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

INVALID_XML_MESSAGE = "Invalid XML file"

def _traverse_xml(tree):
    key = tree.tag
    value = tree.text or ""

    if len(tree) > 0:
        value = [_traverse_xml(child) for child in tree]

    return {key: value}


def parse_xml(raw_xml):
    """
    Converts a raw XML string into a Python dictionary object
    """
    try:
        tree = etree.ElementTree(etree.fromstring(raw_xml))
        root = tree.getroot()
        return _traverse_xml(root)
    except etree.ParseError:
        # XXX maybe throw another error and let the endpoint figure out what to do?
        return INVALID_XML_MESSAGE


class ConverterViewSet(ViewSet):
    parser_classes = [MultiPartParser]

    @action(methods=["POST"], detail=False, url_path="convert")
    def convert(self, request, **kwargs):
        xml_file = request.data.get("file")
        response = parse_xml(xml_file.read())
        status_code = 400 if response == INVALID_XML_MESSAGE else 200
        return Response(response, status=status_code)

